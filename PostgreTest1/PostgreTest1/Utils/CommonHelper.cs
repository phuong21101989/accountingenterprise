﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using PostgreTest1.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace PostgreTest1.Utils
{
    public static class CommonHelper
    {
        public static dynamic GenObjectResponse()
        {
            var result = new ExpandoObject();

            return result;
        }

        public static void setCommonErrorResponse(ref ApiResult apiResult, string message, ILogger logger)
        {
            logger.LogError(message);
            apiResult.status.code = HttpStatusCode.BadGateway;
            apiResult.status.message = message != null && !string.IsNullOrEmpty(message) ? message : "Có lỗi xảy ra!";
            apiResult.data = false;
        }        
    }
}
