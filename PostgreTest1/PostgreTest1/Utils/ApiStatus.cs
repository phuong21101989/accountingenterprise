﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace PostgreTest1.Utils
{
    public class ApiStatus
    {
        private HttpStatusCode _code;
        private string _message;
        private List<string> _messages;

        public HttpStatusCode code
        {
            get => this._code;
            set => this._code = value;
        }

        public string message
        {
            get => this._message;
            set => this._message = value;
        }

        public List<string> messages
        {
            get => this._messages;
            set => this._messages = value;
        }

        public ApiStatus()
        {
            this._code = HttpStatusCode.OK;
            this._message = string.Empty;
            this._messages = (List<string>)null;
        }
    }
}
