﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostgreTest1.Utils
{
    public class ApiResult
    {
        private ApiStatus _status;
        private object _data;

        public ApiStatus status
        {
            get => this._status;
            set => this._status = value;
        }

        public object data
        {
            get => this._data;
            set => this._data = value;
        }

        public ApiResult()
        {
            this._status = new ApiStatus();
            this._data = (object)null;
        }
    }
}
