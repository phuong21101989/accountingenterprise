﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class ReportInfo
    {
        public string Menuid { get; set; }
        public string MaMau { get; set; }
        public string Ctlclass { get; set; }
        public string Spname { get; set; }
        public string Rptname { get; set; }
        public bool? Isdefault { get; set; }
        public string BangChu { get; set; }
        public string BangChu0 { get; set; }
        public bool? Hasnt { get; set; }
        public string Dynreportfields { get; set; }
        public string DescriptionColname { get; set; }
    }
}
