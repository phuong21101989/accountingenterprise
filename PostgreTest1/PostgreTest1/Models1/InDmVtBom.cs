﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class InDmVtBom
    {
        public string Id { get; set; }
        public string MaCty { get; set; }
        public string MaVt { get; set; }
        public string MaLk { get; set; }
        public string TenLk { get; set; }
        public string Dvt { get; set; }
        public decimal? SoLuong { get; set; }
        public decimal? HeSo { get; set; }
        public string GhiChu { get; set; }
        public bool? Ksd { get; set; }
        public DateTime? CDate { get; set; }
        public DateTime? LDate { get; set; }
        public string CUser { get; set; }
        public string LUser { get; set; }
    }
}
