﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Npgsql;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class db_ver10Context : DbContext
    {
        public db_ver10Context()
        {
        }

        public db_ver10Context(DbContextOptions<db_ver10Context> options)
            : base(options)
        {
        }

        public virtual DbSet<CaCt1> CaCt1s { get; set; }
        public virtual DbSet<CaPh1> CaPh1s { get; set; }
        public virtual DbSet<DaoInfo> DaoInfos { get; set; }
        public virtual DbSet<DictionaryInfo> DictionaryInfos { get; set; }
        public virtual DbSet<GlCt> GlCts { get; set; }
        public virtual DbSet<InCt1> InCt1s { get; set; }
        public virtual DbSet<InDmVt> InDmVts { get; set; }
        public virtual DbSet<InDmVtBom> InDmVtBoms { get; set; }
        public virtual DbSet<InDmVtDmKho> InDmVtDmKhos { get; set; }
        public virtual DbSet<InDmVtNhom> InDmVtNhoms { get; set; }
        public virtual DbSet<InDmVtViTri> InDmVtViTris { get; set; }
        public virtual DbSet<InPh1> InPh1s { get; set; }
        public virtual DbSet<MenuInfo> MenuInfos { get; set; }
        public virtual DbSet<ReportInfo> ReportInfos { get; set; }
        public virtual DbSet<SiDmCt> SiDmCts { get; set; }
        public virtual DbSet<ViewFormat> ViewFormats { get; set; }
        public virtual DbSet<VoucherInfo> VoucherInfos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Username=postgres;Password=123456;Database=db_ver1.0;");
            }
        }


        public List<T> ExecSQL<T>(string query)
        {
            using (NpgsqlCommand command = (NpgsqlCommand)Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                Database.OpenConnection();

                List<T> list = new List<T>();
                using (var result = command.ExecuteReader())
                {
                    T obj = default(T);
                    while (result.Read())
                    {
                        obj = Activator.CreateInstance<T>();
                        foreach (PropertyInfo prop in obj.GetType().GetProperties())
                        {
                            if (!object.Equals(result[prop.Name], DBNull.Value))
                            {
                                prop.SetValue(obj, result[prop.Name], null);
                            }
                        }
                        list.Add(obj);
                    }
                }
                Database.CloseConnection();
                return list;
            }
        }

        public IEnumerable<dynamic> ExecSQL2(string query)
        {
            using (NpgsqlCommand command = (NpgsqlCommand)Database.GetDbConnection().CreateCommand())
            {
                command.CommandText = query;
                command.CommandType = CommandType.Text;
                Database.OpenConnection();

                using (var result = command.ExecuteReader())
                {
                    var names = Enumerable.Range(0, result.FieldCount).Select(result.GetName).ToList();
                    foreach (IDataRecord record in result as IEnumerable)
                    {
                        var expando = new ExpandoObject() as IDictionary<string, object>;
                        foreach (var name in names)
                        {
                            expando[name] = record[name];
                            if (record[name].ToString() == string.Empty)
                                expando[name] = "";
                        }

                        yield return expando;
                    }
                }
                Database.CloseConnection();
                //return list;
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Vietnamese_Vietnam.1258");

            modelBuilder.Entity<CaCt1>(entity =>
            {
                entity.ToTable("CaCT1");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.DienGiai)
                    .HasMaxLength(250)
                    .HasColumnName("dien_giai");

                entity.Property(e => e.IdPh)
                    .HasMaxLength(50)
                    .HasColumnName("id_ph");

                entity.Property(e => e.MaBp)
                    .HasMaxLength(20)
                    .HasColumnName("ma_bp");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaHd)
                    .HasMaxLength(20)
                    .HasColumnName("ma_hd");

                entity.Property(e => e.MaKh)
                    .HasMaxLength(20)
                    .HasColumnName("ma_kh");

                entity.Property(e => e.MaKu)
                    .HasMaxLength(20)
                    .HasColumnName("ma_ku");

                entity.Property(e => e.MaLo)
                    .HasMaxLength(20)
                    .HasColumnName("ma_lo");

                entity.Property(e => e.MaPhi)
                    .HasMaxLength(20)
                    .HasColumnName("ma_phi");

                entity.Property(e => e.MaSpct)
                    .HasMaxLength(20)
                    .HasColumnName("ma_spct");

                entity.Property(e => e.PsCo)
                    .HasColumnName("ps_co")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PsCoNt)
                    .HasColumnName("ps_co_nt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PsNo)
                    .HasColumnName("ps_no")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PsNoNt)
                    .HasColumnName("ps_no_nt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Stt)
                    .HasColumnName("stt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Tk)
                    .HasMaxLength(20)
                    .HasColumnName("tk");

                entity.Property(e => e.TyGiaGs)
                    .HasColumnName("ty_gia_gs")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<CaPh1>(entity =>
            {
                entity.ToTable("CaPH1");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.CDate).HasColumnName("cDate");

                entity.Property(e => e.CUser)
                    .HasMaxLength(50)
                    .HasColumnName("cUser");

                entity.Property(e => e.DiaChi)
                    .HasMaxLength(150)
                    .HasColumnName("dia_chi");

                entity.Property(e => e.DienGiai)
                    .HasMaxLength(300)
                    .HasColumnName("dien_giai");

                entity.Property(e => e.LDate).HasColumnName("lDate");

                entity.Property(e => e.LUser)
                    .HasMaxLength(50)
                    .HasColumnName("lUser");

                entity.Property(e => e.MaCt)
                    .HasMaxLength(20)
                    .HasColumnName("ma_ct");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaKh)
                    .HasMaxLength(20)
                    .HasColumnName("ma_kh");

                entity.Property(e => e.MaNt)
                    .HasMaxLength(3)
                    .HasColumnName("ma_nt");

                entity.Property(e => e.NgayCt).HasColumnName("ngay_ct");

                entity.Property(e => e.NgayLct).HasColumnName("ngay_lct");

                entity.Property(e => e.NguoiGd)
                    .HasMaxLength(100)
                    .HasColumnName("nguoi_gd");

                entity.Property(e => e.Post2gl)
                    .HasColumnName("post2gl")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.SoCt)
                    .HasMaxLength(20)
                    .HasColumnName("so_ct");

                entity.Property(e => e.TTien)
                    .HasColumnName("t_tien")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TTienNt)
                    .HasColumnName("t_tien_nt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TenKh)
                    .HasMaxLength(150)
                    .HasColumnName("ten_kh");

                entity.Property(e => e.TkNo)
                    .HasMaxLength(20)
                    .HasColumnName("tk_no");

                entity.Property(e => e.TrangThai)
                    .HasMaxLength(1)
                    .HasColumnName("trang_thai");

                entity.Property(e => e.TyGia)
                    .HasColumnName("ty_gia")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<DaoInfo>(entity =>
            {
                entity.HasKey(e => e.TableName)
                    .HasName("DaoInfo_pkey");

                entity.ToTable("DaoInfo");

                entity.HasComment("Khai báo thông tin các hàm thao tác với danh mục");

                entity.Property(e => e.TableName)
                    .HasMaxLength(100)
                    .HasColumnName("table_name");

                entity.Property(e => e.ChgSp)
                    .HasMaxLength(100)
                    .HasColumnName("chg_sp");

                entity.Property(e => e.DelSp)
                    .HasMaxLength(100)
                    .HasColumnName("del_sp");

                entity.Property(e => e.Description)
                    .HasMaxLength(255)
                    .HasColumnName("description");

                entity.Property(e => e.GetSp)
                    .HasMaxLength(100)
                    .HasColumnName("get_sp");

                entity.Property(e => e.InsSp)
                    .HasMaxLength(100)
                    .HasColumnName("ins_sp");

                entity.Property(e => e.SchSp)
                    .HasMaxLength(100)
                    .HasColumnName("sch_sp");

                entity.Property(e => e.UpdSp)
                    .HasMaxLength(100)
                    .HasColumnName("upd_sp");
            });

            modelBuilder.Entity<DictionaryInfo>(entity =>
            {
                entity.HasKey(e => e.CodeName)
                    .HasName("DictionaryInfo_pkey");

                entity.ToTable("DictionaryInfo");

                entity.HasComment("Khai báo thông tin danh mục");

                entity.Property(e => e.CodeName)
                    .HasMaxLength(50)
                    .HasColumnName("code_name");

                entity.Property(e => e.AllowMergeCode).HasColumnName("allow_merge_code");

                entity.Property(e => e.AutoCreateCode).HasColumnName("auto_create_code");

                entity.Property(e => e.CarryFieldList)
                    .HasMaxLength(255)
                    .HasColumnName("carry_field_list");

                entity.Property(e => e.CodeFname)
                    .HasMaxLength(50)
                    .HasColumnName("code_fname");

                entity.Property(e => e.CodeLength).HasColumnName("code_length");

                entity.Property(e => e.ColumnCheckExists)
                    .HasMaxLength(200)
                    .HasColumnName("column_check_exists");

                entity.Property(e => e.ColumnCheckRelate)
                    .HasMaxLength(200)
                    .HasColumnName("column_check_relate");

                entity.Property(e => e.ColumnCheckRequire)
                    .HasMaxLength(200)
                    .HasColumnName("column_check_require");

                entity.Property(e => e.CopyVaora).HasColumnName("copy_vaora");

                entity.Property(e => e.DefaultSort)
                    .HasMaxLength(128)
                    .HasColumnName("default_sort");

                entity.Property(e => e.Description)
                    .HasMaxLength(128)
                    .HasColumnName("description");

                entity.Property(e => e.Dllname)
                    .HasMaxLength(100)
                    .HasColumnName("dllname");

                entity.Property(e => e.EditClassName)
                    .HasMaxLength(50)
                    .HasColumnName("edit_class_name");

                entity.Property(e => e.LookupWhenInvalid).HasColumnName("lookup_when_invalid");

                entity.Property(e => e.Menuid)
                    .HasMaxLength(20)
                    .HasColumnName("menuid");

                entity.Property(e => e.NameFname)
                    .HasMaxLength(50)
                    .HasColumnName("name_fname");

                entity.Property(e => e.Pk)
                    .HasMaxLength(128)
                    .HasColumnName("PK");

                entity.Property(e => e.Prefix)
                    .HasMaxLength(10)
                    .HasColumnName("prefix");

                entity.Property(e => e.TableName)
                    .HasMaxLength(100)
                    .HasColumnName("table_name");

                entity.Property(e => e.ViewClassName)
                    .HasMaxLength(50)
                    .HasColumnName("view_class_name");
            });

            modelBuilder.Entity<GlCt>(entity =>
            {
                entity.HasKey(e => new { e.MaCty, e.IdPh, e.Stt })
                    .HasName("GlCt_pkey");

                entity.ToTable("GlCt");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.IdPh)
                    .HasMaxLength(50)
                    .HasColumnName("id_ph");

                entity.Property(e => e.Stt).HasColumnName("stt");

                entity.Property(e => e.Cdate).HasColumnName("cdate");

                entity.Property(e => e.Cuser)
                    .HasMaxLength(50)
                    .HasColumnName("cuser");

                entity.Property(e => e.DienGiai)
                    .HasMaxLength(300)
                    .HasColumnName("dien_giai");

                entity.Property(e => e.Ldate).HasColumnName("ldate");

                entity.Property(e => e.Luser)
                    .HasMaxLength(50)
                    .HasColumnName("luser");

                entity.Property(e => e.MaBp)
                    .HasMaxLength(20)
                    .HasColumnName("ma_bp");

                entity.Property(e => e.MaCt)
                    .HasMaxLength(20)
                    .HasColumnName("ma_ct");

                entity.Property(e => e.MaHd)
                    .HasMaxLength(20)
                    .HasColumnName("ma_hd");

                entity.Property(e => e.MaKh)
                    .HasMaxLength(20)
                    .HasColumnName("ma_kh");

                entity.Property(e => e.MaKu)
                    .HasMaxLength(20)
                    .HasColumnName("ma_ku");

                entity.Property(e => e.MaLo)
                    .HasMaxLength(20)
                    .HasColumnName("ma_lo");

                entity.Property(e => e.MaNt)
                    .HasMaxLength(3)
                    .HasColumnName("ma_nt");

                entity.Property(e => e.MaNvkd)
                    .HasMaxLength(20)
                    .HasColumnName("ma_nvkd");

                entity.Property(e => e.MaNx)
                    .HasMaxLength(20)
                    .HasColumnName("ma_nx");

                entity.Property(e => e.MaPhi)
                    .HasMaxLength(20)
                    .HasColumnName("ma_phi");

                entity.Property(e => e.MaSpct)
                    .HasMaxLength(20)
                    .HasColumnName("ma_spct");

                entity.Property(e => e.Nam)
                    .HasColumnName("nam")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.NgayCt).HasColumnName("ngay_ct");

                entity.Property(e => e.NgayLct).HasColumnName("ngay_lct");

                entity.Property(e => e.NgayLo).HasColumnName("ngay_lo");

                entity.Property(e => e.NguoiGd)
                    .HasMaxLength(50)
                    .HasColumnName("nguoi_gd");

                entity.Property(e => e.NhomDk)
                    .HasMaxLength(3)
                    .HasColumnName("nhom_dk");

                entity.Property(e => e.PsCo)
                    .HasColumnName("ps_co")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PsCoNt)
                    .HasColumnName("ps_co_nt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PsNo)
                    .HasColumnName("ps_no")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.PsNoNt)
                    .HasColumnName("ps_no_nt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SoCt)
                    .HasMaxLength(20)
                    .HasColumnName("so_ct");

                entity.Property(e => e.SoLo)
                    .HasMaxLength(20)
                    .HasColumnName("so_lo");

                entity.Property(e => e.Thang)
                    .HasColumnName("thang")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Tk)
                    .HasMaxLength(20)
                    .HasColumnName("tk");

                entity.Property(e => e.TkDu)
                    .HasMaxLength(20)
                    .HasColumnName("tk_du");

                entity.Property(e => e.TyGia)
                    .HasColumnName("ty_gia")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<InCt1>(entity =>
            {
                entity.ToTable("InCT1");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.Dvt)
                    .HasMaxLength(20)
                    .HasColumnName("dvt");

                entity.Property(e => e.Gia)
                    .HasColumnName("gia")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.GiaNt)
                    .HasColumnName("gia_nt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.IdLsx)
                    .HasMaxLength(50)
                    .HasColumnName("id_lsx");

                entity.Property(e => e.IdPh)
                    .HasMaxLength(50)
                    .HasColumnName("id_ph");

                entity.Property(e => e.MaBp)
                    .HasMaxLength(20)
                    .HasColumnName("ma_bp");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaHd)
                    .HasMaxLength(20)
                    .HasColumnName("ma_hd");

                entity.Property(e => e.MaKho)
                    .HasMaxLength(20)
                    .HasColumnName("ma_kho");

                entity.Property(e => e.MaLo)
                    .HasMaxLength(20)
                    .HasColumnName("ma_lo");

                entity.Property(e => e.MaNx)
                    .HasMaxLength(20)
                    .HasColumnName("ma_nx");

                entity.Property(e => e.MaPhi)
                    .HasMaxLength(20)
                    .HasColumnName("ma_phi");

                entity.Property(e => e.MaSpct)
                    .HasMaxLength(20)
                    .HasColumnName("ma_spct");

                entity.Property(e => e.MaVitri)
                    .HasMaxLength(20)
                    .HasColumnName("ma_vitri");

                entity.Property(e => e.MaVt)
                    .HasMaxLength(20)
                    .HasColumnName("ma_vt");

                entity.Property(e => e.NgayCt).HasColumnName("ngay_ct");

                entity.Property(e => e.SoLsx)
                    .HasMaxLength(12)
                    .HasColumnName("so_lsx");

                entity.Property(e => e.SoLuong)
                    .HasColumnName("so_luong")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SoLuongQd)
                    .HasColumnName("so_luong_qd")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Stt)
                    .HasColumnName("stt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SttLsx)
                    .HasColumnName("stt_lsx")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TenVt)
                    .HasMaxLength(200)
                    .HasColumnName("ten_vt");

                entity.Property(e => e.Tien)
                    .HasColumnName("tien")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TienNt)
                    .HasColumnName("tien_nt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TkCo)
                    .HasMaxLength(20)
                    .HasColumnName("tk_co");

                entity.Property(e => e.TkVt)
                    .HasMaxLength(20)
                    .HasColumnName("tk_vt");
            });

            modelBuilder.Entity<InDmVt>(entity =>
            {
                entity.ToTable("InDmVt");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.CDate).HasColumnName("cDate");

                entity.Property(e => e.CUser)
                    .HasMaxLength(50)
                    .HasColumnName("cUser");

                entity.Property(e => e.CoCt)
                    .HasColumnName("co_ct")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Dvt)
                    .HasMaxLength(20)
                    .HasColumnName("dvt");

                entity.Property(e => e.DvtBan)
                    .HasMaxLength(20)
                    .HasColumnName("dvt_ban");

                entity.Property(e => e.DvtMua)
                    .HasMaxLength(20)
                    .HasColumnName("dvt_mua");

                entity.Property(e => e.GhiChu)
                    .HasMaxLength(300)
                    .HasColumnName("ghi_chu");

                entity.Property(e => e.GiaTon)
                    .HasMaxLength(1)
                    .HasColumnName("gia_ton");

                entity.Property(e => e.HsDvtban)
                    .HasColumnName("hs_dvtban")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.HsDvtmua)
                    .HasColumnName("hs_dvtmua")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Ksd)
                    .HasColumnName("ksd")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.LDate).HasColumnName("lDate");

                entity.Property(e => e.LUser)
                    .HasMaxLength(50)
                    .HasColumnName("lUser");

                entity.Property(e => e.Loai)
                    .HasMaxLength(1)
                    .HasColumnName("loai");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaKho)
                    .HasMaxLength(20)
                    .HasColumnName("ma_kho");

                entity.Property(e => e.MaNhvat)
                    .HasMaxLength(20)
                    .HasColumnName("ma_nhvat");

                entity.Property(e => e.MaNhvt)
                    .HasMaxLength(20)
                    .HasColumnName("ma_nhvt");

                entity.Property(e => e.MaPlvt1)
                    .HasMaxLength(20)
                    .HasColumnName("ma_plvt1");

                entity.Property(e => e.MaPlvt2)
                    .HasMaxLength(20)
                    .HasColumnName("ma_plvt2");

                entity.Property(e => e.MaPlvt3)
                    .HasMaxLength(20)
                    .HasColumnName("ma_plvt3");

                entity.Property(e => e.MaThue)
                    .HasMaxLength(20)
                    .HasColumnName("ma_thue");

                entity.Property(e => e.MaVitri)
                    .HasMaxLength(20)
                    .HasColumnName("ma_vitri");

                entity.Property(e => e.MaVt)
                    .HasMaxLength(20)
                    .HasColumnName("ma_vt");

                entity.Property(e => e.NhaPp)
                    .HasMaxLength(150)
                    .HasColumnName("nha_pp");

                entity.Property(e => e.NhaSx)
                    .HasMaxLength(150)
                    .HasColumnName("nha_sx");

                entity.Property(e => e.NuocSx)
                    .HasMaxLength(100)
                    .HasColumnName("nuoc_sx");

                entity.Property(e => e.PartNo)
                    .HasMaxLength(20)
                    .HasColumnName("part_no");

                entity.Property(e => e.SlMax)
                    .HasColumnName("sl_max")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SlMin)
                    .HasColumnName("sl_min")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TenVt)
                    .HasMaxLength(200)
                    .HasColumnName("ten_vt");

                entity.Property(e => e.TinhGt)
                    .HasColumnName("tinh_gt")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.TkCk)
                    .HasMaxLength(20)
                    .HasColumnName("tk_ck");

                entity.Property(e => e.TkCpnvl)
                    .HasMaxLength(20)
                    .HasColumnName("tk_cpnvl");

                entity.Property(e => e.TkDd)
                    .HasMaxLength(20)
                    .HasColumnName("tk_dd");

                entity.Property(e => e.TkDt)
                    .HasMaxLength(20)
                    .HasColumnName("tk_dt");

                entity.Property(e => e.TkDtnb)
                    .HasMaxLength(20)
                    .HasColumnName("tk_dtnb");

                entity.Property(e => e.TkGv)
                    .HasMaxLength(20)
                    .HasColumnName("tk_gv");

                entity.Property(e => e.TkKm)
                    .HasMaxLength(20)
                    .HasColumnName("tk_km");

                entity.Property(e => e.TkTl)
                    .HasMaxLength(20)
                    .HasColumnName("tk_tl");

                entity.Property(e => e.TkVt)
                    .HasMaxLength(20)
                    .HasColumnName("tk_vt");

                entity.Property(e => e.TonKho)
                    .HasColumnName("ton_kho")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.TsGtgt)
                    .HasColumnName("ts_gtgt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TsNk)
                    .HasColumnName("ts_nk")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TsTtdb)
                    .HasColumnName("ts_ttdb")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TsXk)
                    .HasColumnName("ts_xk")
                    .HasDefaultValueSql("0");
            });

            modelBuilder.Entity<InDmVtBom>(entity =>
            {
                entity.ToTable("InDmVt_BOM");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.CDate).HasColumnName("cDate");

                entity.Property(e => e.CUser)
                    .HasMaxLength(50)
                    .HasColumnName("cUser");

                entity.Property(e => e.Dvt)
                    .HasMaxLength(20)
                    .HasColumnName("dvt");

                entity.Property(e => e.GhiChu)
                    .HasMaxLength(255)
                    .HasColumnName("ghi_chu");

                entity.Property(e => e.HeSo)
                    .HasColumnName("he_so")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Ksd).HasColumnName("ksd");

                entity.Property(e => e.LDate).HasColumnName("lDate");

                entity.Property(e => e.LUser)
                    .HasMaxLength(50)
                    .HasColumnName("lUser");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaLk)
                    .HasMaxLength(20)
                    .HasColumnName("ma_lk");

                entity.Property(e => e.MaVt)
                    .HasMaxLength(20)
                    .HasColumnName("ma_vt");

                entity.Property(e => e.SoLuong)
                    .HasColumnName("so_luong")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TenLk)
                    .HasMaxLength(100)
                    .HasColumnName("ten_lk");
            });

            modelBuilder.Entity<InDmVtDmKho>(entity =>
            {
                entity.ToTable("InDmVt_DmKho");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.CDate).HasColumnName("cDate");

                entity.Property(e => e.CUser)
                    .HasMaxLength(50)
                    .HasColumnName("cUser");

                entity.Property(e => e.DiaChi)
                    .HasMaxLength(150)
                    .HasColumnName("dia_chi");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email");

                entity.Property(e => e.Fax)
                    .HasMaxLength(50)
                    .HasColumnName("fax");

                entity.Property(e => e.KhoDl).HasColumnName("kho_dl");

                entity.Property(e => e.Ksd).HasColumnName("ksd");

                entity.Property(e => e.LDate).HasColumnName("lDate");

                entity.Property(e => e.LUser)
                    .HasMaxLength(50)
                    .HasColumnName("lUser");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaKho)
                    .HasMaxLength(20)
                    .HasColumnName("ma_kho");

                entity.Property(e => e.NguoiLh)
                    .HasMaxLength(50)
                    .HasColumnName("nguoi_lh");

                entity.Property(e => e.SttNtxt).HasColumnName("stt_ntxt");

                entity.Property(e => e.Tel)
                    .HasMaxLength(50)
                    .HasColumnName("tel");

                entity.Property(e => e.TenKho)
                    .HasMaxLength(100)
                    .HasColumnName("ten_kho");

                entity.Property(e => e.TkDl)
                    .HasMaxLength(20)
                    .HasColumnName("tk_dl");
            });

            modelBuilder.Entity<InDmVtNhom>(entity =>
            {
                entity.ToTable("InDmVt_Nhom");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.CDate).HasColumnName("cDate");

                entity.Property(e => e.CUser)
                    .HasMaxLength(50)
                    .HasColumnName("cUser");

                entity.Property(e => e.Cap).HasColumnName("cap");

                entity.Property(e => e.CongSl).HasColumnName("cong_sl");

                entity.Property(e => e.Ksd).HasColumnName("ksd");

                entity.Property(e => e.LDate).HasColumnName("lDate");

                entity.Property(e => e.LUser)
                    .HasMaxLength(50)
                    .HasColumnName("lUser");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaNhvt)
                    .HasMaxLength(20)
                    .HasColumnName("ma_nhvt");

                entity.Property(e => e.NhomMe)
                    .HasMaxLength(20)
                    .HasColumnName("nhom_me");

                entity.Property(e => e.SttNhom)
                    .HasMaxLength(18)
                    .HasColumnName("stt_nhom");

                entity.Property(e => e.TenNhvt)
                    .HasMaxLength(100)
                    .HasColumnName("ten_nhvt");

                entity.Property(e => e.TkCk)
                    .HasMaxLength(20)
                    .HasColumnName("tk_ck");

                entity.Property(e => e.TkDt)
                    .HasMaxLength(20)
                    .HasColumnName("tk_dt");

                entity.Property(e => e.TkDtnb)
                    .HasMaxLength(20)
                    .HasColumnName("tk_dtnb");

                entity.Property(e => e.TkGv)
                    .HasMaxLength(20)
                    .HasColumnName("tk_gv");

                entity.Property(e => e.TkTl)
                    .HasMaxLength(20)
                    .HasColumnName("tk_tl");

                entity.Property(e => e.TkVt)
                    .HasMaxLength(20)
                    .HasColumnName("tk_vt");
            });

            modelBuilder.Entity<InDmVtViTri>(entity =>
            {
                entity.ToTable("InDmVt_ViTri");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.CDate).HasColumnName("cDate");

                entity.Property(e => e.CUser)
                    .HasMaxLength(50)
                    .HasColumnName("cUser");

                entity.Property(e => e.Ksd).HasColumnName("ksd");

                entity.Property(e => e.LDate).HasColumnName("lDate");

                entity.Property(e => e.LUser)
                    .HasMaxLength(50)
                    .HasColumnName("lUser");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaKho)
                    .HasMaxLength(20)
                    .HasColumnName("ma_kho");

                entity.Property(e => e.MaVitri)
                    .HasMaxLength(20)
                    .HasColumnName("ma_vitri");

                entity.Property(e => e.TenVitri)
                    .HasMaxLength(100)
                    .HasColumnName("ten_vitri");
            });

            modelBuilder.Entity<InPh1>(entity =>
            {
                entity.ToTable("InPH1");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.CDate).HasColumnName("cDate");

                entity.Property(e => e.CUser)
                    .HasMaxLength(50)
                    .HasColumnName("cUser");

                entity.Property(e => e.DiaChi)
                    .HasMaxLength(150)
                    .HasColumnName("dia_chi");

                entity.Property(e => e.DienGiai)
                    .HasMaxLength(300)
                    .HasColumnName("dien_giai");

                entity.Property(e => e.GiaThanh)
                    .HasColumnName("gia_thanh")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.LDate).HasColumnName("lDate");

                entity.Property(e => e.LUser)
                    .HasMaxLength(50)
                    .HasColumnName("lUser");

                entity.Property(e => e.MaBp)
                    .HasMaxLength(20)
                    .HasColumnName("ma_bp");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaGd)
                    .HasMaxLength(1)
                    .HasColumnName("ma_gd");

                entity.Property(e => e.MaKh)
                    .HasMaxLength(20)
                    .HasColumnName("ma_kh");

                entity.Property(e => e.MaNt)
                    .HasMaxLength(20)
                    .HasColumnName("ma_nt");

                entity.Property(e => e.MaNx)
                    .HasMaxLength(20)
                    .HasColumnName("ma_nx");

                entity.Property(e => e.MaPhi)
                    .HasMaxLength(20)
                    .HasColumnName("ma_phi");

                entity.Property(e => e.NgayCt).HasColumnName("ngay_ct");

                entity.Property(e => e.NgayLct).HasColumnName("ngay_lct");

                entity.Property(e => e.NguoiGd)
                    .HasMaxLength(100)
                    .HasColumnName("nguoi_gd");

                entity.Property(e => e.PnGtb)
                    .HasColumnName("pn_gtb")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Post2gl)
                    .HasColumnName("post2gl")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Post2in)
                    .HasColumnName("post2in")
                    .HasDefaultValueSql("true");

                entity.Property(e => e.SoCt)
                    .HasMaxLength(20)
                    .HasColumnName("so_ct");

                entity.Property(e => e.TSoLuong)
                    .HasColumnName("t_so_luong")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TTien)
                    .HasColumnName("t_tien")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TTienNt)
                    .HasColumnName("t_tien_nt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TenKh)
                    .HasMaxLength(150)
                    .HasColumnName("ten_kh");

                entity.Property(e => e.TrangThai)
                    .HasMaxLength(1)
                    .HasColumnName("trang_thai");

                entity.Property(e => e.TyGia)
                    .HasColumnName("ty_gia")
                    .HasDefaultValueSql("1");
            });

            modelBuilder.Entity<MenuInfo>(entity =>
            {
                entity.HasKey(e => e.Menuid)
                    .HasName("MenuInfo_pkey");

                entity.ToTable("MenuInfo");

                entity.Property(e => e.Menuid)
                    .HasMaxLength(8)
                    .HasColumnName("menuid");

                entity.Property(e => e.Active)
                    .HasMaxLength(1)
                    .HasColumnName("active");

                entity.Property(e => e.AngularController)
                    .HasMaxLength(200)
                    .HasColumnName("angular_controller");

                entity.Property(e => e.AngularFilter)
                    .HasMaxLength(100)
                    .HasColumnName("angular_filter");

                entity.Property(e => e.AngularRoute)
                    .HasMaxLength(100)
                    .HasColumnName("angular_route");

                entity.Property(e => e.AngularTemplate)
                    .HasMaxLength(200)
                    .HasColumnName("angular_template");

                entity.Property(e => e.Bar)
                    .HasMaxLength(200)
                    .HasColumnName("bar");

                entity.Property(e => e.Basicright).HasColumnName("basicright");

                entity.Property(e => e.CodeName)
                    .HasMaxLength(50)
                    .HasColumnName("code_name");

                entity.Property(e => e.Command)
                    .HasMaxLength(200)
                    .HasColumnName("command");

                entity.Property(e => e.DllName)
                    .HasMaxLength(50)
                    .HasColumnName("dllName");

                entity.Property(e => e.Form).HasColumnName("form");

                entity.Property(e => e.FormShowType)
                    .HasMaxLength(1)
                    .HasColumnName("form_show_type");

                entity.Property(e => e.Moduleid)
                    .HasMaxLength(5)
                    .HasColumnName("moduleid");

                entity.Property(e => e.Opt)
                    .HasMaxLength(1)
                    .HasColumnName("opt");

                entity.Property(e => e.Picture1)
                    .HasMaxLength(200)
                    .HasColumnName("picture1");

                entity.Property(e => e.Picture2)
                    .HasMaxLength(200)
                    .HasColumnName("picture2");

                entity.Property(e => e.Report).HasColumnName("report");

                entity.Property(e => e.Rowid).HasColumnName("rowid");

                entity.Property(e => e.ShortName)
                    .HasMaxLength(200)
                    .HasColumnName("short_name");

                entity.Property(e => e.Stt)
                    .HasMaxLength(12)
                    .HasColumnName("stt");

                entity.Property(e => e.Type)
                    .HasMaxLength(1)
                    .HasColumnName("type");

                entity.Property(e => e.Used).HasColumnName("used");
            });

            modelBuilder.Entity<ReportInfo>(entity =>
            {
                entity.HasKey(e => new { e.Menuid, e.MaMau })
                    .HasName("ReportInfo_pkey");

                entity.ToTable("ReportInfo");

                entity.HasComment("Khai báo thông tin của các báo cáo");

                entity.Property(e => e.Menuid)
                    .HasMaxLength(8)
                    .HasColumnName("menuid");

                entity.Property(e => e.MaMau)
                    .HasMaxLength(20)
                    .HasColumnName("ma_mau");

                entity.Property(e => e.BangChu)
                    .HasMaxLength(50)
                    .HasColumnName("bang_chu");

                entity.Property(e => e.BangChu0)
                    .HasMaxLength(50)
                    .HasColumnName("bang_chu0");

                entity.Property(e => e.Ctlclass)
                    .HasMaxLength(50)
                    .HasColumnName("ctlclass");

                entity.Property(e => e.DescriptionColname)
                    .HasMaxLength(100)
                    .HasColumnName("description_colname");

                entity.Property(e => e.Dynreportfields)
                    .HasMaxLength(4000)
                    .HasColumnName("dynreportfields");

                entity.Property(e => e.Hasnt)
                    .HasColumnName("hasnt")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Isdefault)
                    .HasColumnName("isdefault")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Rptname)
                    .HasMaxLength(50)
                    .HasColumnName("rptname");

                entity.Property(e => e.Spname)
                    .HasMaxLength(50)
                    .HasColumnName("spname");
            });

            modelBuilder.Entity<SiDmCt>(entity =>
            {
                entity.HasKey(e => new { e.MaCty, e.MaCt })
                    .HasName("SiDmCt_pkey");

                entity.ToTable("SiDmCt");

                entity.Property(e => e.MaCty)
                    .HasMaxLength(5)
                    .HasColumnName("ma_cty");

                entity.Property(e => e.MaCt)
                    .HasMaxLength(3)
                    .HasColumnName("ma_ct");

                entity.Property(e => e.AllowReorderSoCt)
                    .HasColumnName("allow_reorder_so_ct")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Bp)
                    .HasColumnName("bp")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Ct)
                    .HasMaxLength(30)
                    .HasColumnName("ct");

                entity.Property(e => e.CtDc)
                    .HasColumnName("ct_dc")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.CtDsLookupKhiRong)
                    .HasMaxLength(200)
                    .HasColumnName("ct_ds_lookup_khi_rong");

                entity.Property(e => e.Ctfieldlist2in)
                    .HasMaxLength(1000)
                    .HasColumnName("ctfieldlist2in");

                entity.Property(e => e.Hddt)
                    .HasColumnName("hddt")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.KieuTrungSoCt)
                    .HasMaxLength(1)
                    .HasColumnName("kieu_trung_so_ct");

                entity.Property(e => e.Lo)
                    .HasColumnName("lo")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.LocNsd)
                    .HasColumnName("loc_nsd")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.MaCtMe)
                    .HasMaxLength(3)
                    .HasColumnName("ma_ct_me");

                entity.Property(e => e.MaNt)
                    .HasMaxLength(3)
                    .HasColumnName("ma_nt");

                entity.Property(e => e.Menuid)
                    .HasMaxLength(18)
                    .HasColumnName("menuid");

                entity.Property(e => e.Nxt)
                    .HasMaxLength(1)
                    .HasColumnName("nxt");

                entity.Property(e => e.Ph)
                    .HasMaxLength(30)
                    .HasColumnName("ph");

                entity.Property(e => e.PhDsLookupKhiRong)
                    .HasMaxLength(200)
                    .HasColumnName("ph_ds_lookup_khi_rong");

                entity.Property(e => e.PhanHe)
                    .HasMaxLength(3)
                    .HasColumnName("phan_he");

                entity.Property(e => e.Phfieldlist2in)
                    .HasMaxLength(1000)
                    .HasColumnName("phfieldlist2in");

                entity.Property(e => e.Phi)
                    .HasColumnName("phi")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.Sd)
                    .HasColumnName("sd")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.SoLien)
                    .HasColumnName("so_lien")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SpPost)
                    .HasMaxLength(50)
                    .HasColumnName("sp_post");

                entity.Property(e => e.SpProcess)
                    .HasMaxLength(50)
                    .HasColumnName("sp_process");

                entity.Property(e => e.Spct)
                    .HasColumnName("spct")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.SttNkc)
                    .HasColumnName("stt_nkc")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SttNtxt)
                    .HasColumnName("stt_ntxt")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.TenCt)
                    .HasMaxLength(100)
                    .HasColumnName("ten_ct");

                entity.Property(e => e.TkCo)
                    .HasMaxLength(20)
                    .HasColumnName("tk_co");

                entity.Property(e => e.TkNo)
                    .HasMaxLength(20)
                    .HasColumnName("tk_no");

                entity.Property(e => e.VnPattern)
                    .HasMaxLength(1)
                    .HasColumnName("vn_pattern");

                entity.Property(e => e.VnPostfix)
                    .HasMaxLength(12)
                    .HasColumnName("vn_postfix");

                entity.Property(e => e.VnPrefix)
                    .HasMaxLength(12)
                    .HasColumnName("vn_prefix");

                entity.Property(e => e.VnSequence)
                    .HasColumnName("vn_sequence")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.VnWidth)
                    .HasColumnName("vn_width")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Vv)
                    .HasColumnName("vv")
                    .HasDefaultValueSql("false");
            });

            modelBuilder.Entity<ViewFormat>(entity =>
            {
                entity.ToTable("ViewFormat");

                entity.HasComment("Khai báo thông tin các cột hiển thị trên danh sách");

                entity.Property(e => e.Id)
                    .HasMaxLength(50)
                    .HasColumnName("id");

                entity.Property(e => e.CodeName)
                    .HasMaxLength(20)
                    .HasColumnName("code_name");

                entity.Property(e => e.DefaultSort).HasColumnName("default_sort");

                entity.Property(e => e.FieldFormat)
                    .HasMaxLength(50)
                    .HasColumnName("field_format");

                entity.Property(e => e.FieldHide).HasColumnName("field_hide");

                entity.Property(e => e.FieldName)
                    .HasMaxLength(50)
                    .HasColumnName("field_name");

                entity.Property(e => e.FieldOrder).HasColumnName("field_order");

                entity.Property(e => e.FieldTitle)
                    .HasMaxLength(255)
                    .HasColumnName("field_title");

                entity.Property(e => e.FieldType)
                    .HasMaxLength(50)
                    .HasColumnName("field_type");

                entity.Property(e => e.FieldWidth).HasColumnName("field_width");

                entity.Property(e => e.Language)
                    .HasMaxLength(20)
                    .HasColumnName("language");

                entity.Property(e => e.Locked).HasColumnName("locked");

                entity.Property(e => e.MaMau)
                    .HasMaxLength(10)
                    .HasColumnName("ma_mau");

                entity.Property(e => e.Menuid)
                    .HasMaxLength(10)
                    .HasColumnName("menuid");

                entity.Property(e => e.Type)
                    .HasMaxLength(10)
                    .HasColumnName("type");

                entity.Property(e => e.ViewType).HasColumnName("view_type");
            });

            modelBuilder.Entity<VoucherInfo>(entity =>
            {
                entity.HasKey(e => e.VoucherCode)
                    .HasName("VoucherInfo_pkey");

                entity.ToTable("VoucherInfo");

                entity.HasComment("Khai báo thông tin của các chứng từ");

                entity.Property(e => e.VoucherCode)
                    .HasMaxLength(50)
                    .HasColumnName("voucher_code");

                entity.Property(e => e.CopyEnabled)
                    .HasColumnName("copy_enabled")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.CopyVaora)
                    .HasColumnName("copy_vaora")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.CpTableName)
                    .HasMaxLength(100)
                    .HasColumnName("cp_table_name");

                entity.Property(e => e.CtCarryFieldList)
                    .HasMaxLength(100)
                    .HasColumnName("ct_carry_field_list");

                entity.Property(e => e.CtTableName)
                    .HasMaxLength(100)
                    .HasColumnName("ct_table_name");

                entity.Property(e => e.Description)
                    .HasMaxLength(50)
                    .HasColumnName("description");

                entity.Property(e => e.HasCp)
                    .HasColumnName("has_cp")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.HasTain)
                    .HasColumnName("has_tain")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.HasTaout)
                    .HasColumnName("has_taout")
                    .HasDefaultValueSql("false");

                entity.Property(e => e.MaNtDefault)
                    .HasMaxLength(3)
                    .HasColumnName("ma_nt_default");

                entity.Property(e => e.Menuid)
                    .HasMaxLength(8)
                    .HasColumnName("menuid");

                entity.Property(e => e.NumberOfCopy)
                    .HasColumnName("number_of_copy")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.Nxt)
                    .HasMaxLength(1)
                    .HasColumnName("nxt");

                entity.Property(e => e.PhCarryFieldList)
                    .HasMaxLength(100)
                    .HasColumnName("ph_carry_field_list");

                entity.Property(e => e.PhExtraEditCotrolList)
                    .HasMaxLength(300)
                    .HasColumnName("ph_extra_edit_cotrol_list");

                entity.Property(e => e.PhPrintFieldList)
                    .HasMaxLength(500)
                    .HasColumnName("ph_print_field_list");

                entity.Property(e => e.PhTableName)
                    .HasMaxLength(100)
                    .HasColumnName("ph_table_name");

                entity.Property(e => e.PrintClassName)
                    .HasMaxLength(50)
                    .HasColumnName("print_class_name");

                entity.Property(e => e.RowPerPage)
                    .HasColumnName("row_per_page")
                    .HasDefaultValueSql("0");

                entity.Property(e => e.SearchClassName)
                    .HasMaxLength(50)
                    .HasColumnName("search_class_name");

                entity.Property(e => e.ViewClassName)
                    .HasMaxLength(50)
                    .HasColumnName("view_class_name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
