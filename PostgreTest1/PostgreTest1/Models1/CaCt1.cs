﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class CaCt1
    {
        public string Id { get; set; }
        public string MaCty { get; set; }
        public string IdPh { get; set; }
        public int? Stt { get; set; }
        public string Tk { get; set; }
        public decimal? PsNo { get; set; }
        public decimal? PsCo { get; set; }
        public decimal? PsNoNt { get; set; }
        public decimal? PsCoNt { get; set; }
        public decimal? TyGiaGs { get; set; }
        public string DienGiai { get; set; }
        public string MaKh { get; set; }
        public string MaHd { get; set; }
        public string MaBp { get; set; }
        public string MaPhi { get; set; }
        public string MaKu { get; set; }
        public string MaSpct { get; set; }
        public string MaLo { get; set; }
    }
}
