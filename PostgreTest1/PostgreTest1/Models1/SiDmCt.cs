﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class SiDmCt
    {
        public string MaCty { get; set; }
        public string MaCt { get; set; }
        public string PhanHe { get; set; }
        public string MaCtMe { get; set; }
        public string TenCt { get; set; }
        public string TkNo { get; set; }
        public string TkCo { get; set; }
        public string MaNt { get; set; }
        public int? SoLien { get; set; }
        public int? SttNkc { get; set; }
        public int? SttNtxt { get; set; }
        public int? CtDc { get; set; }
        public bool? LocNsd { get; set; }
        public bool? Vv { get; set; }
        public bool? Spct { get; set; }
        public bool? Phi { get; set; }
        public bool? Bp { get; set; }
        public bool? Lo { get; set; }
        public bool? Sd { get; set; }
        public bool? AllowReorderSoCt { get; set; }
        public bool? Hddt { get; set; }
        public string SpPost { get; set; }
        public string SpProcess { get; set; }
        public string Ph { get; set; }
        public string Ct { get; set; }
        public string Nxt { get; set; }
        public string Menuid { get; set; }
        public string VnPrefix { get; set; }
        public string VnPostfix { get; set; }
        public long? VnSequence { get; set; }
        public string VnPattern { get; set; }
        public int? VnWidth { get; set; }
        public string Phfieldlist2in { get; set; }
        public string Ctfieldlist2in { get; set; }
        public string KieuTrungSoCt { get; set; }
        public string PhDsLookupKhiRong { get; set; }
        public string CtDsLookupKhiRong { get; set; }
    }
}
