﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class InDmVtNhom
    {
        public string Id { get; set; }
        public string MaCty { get; set; }
        public string MaNhvt { get; set; }
        public string TenNhvt { get; set; }
        public int? Cap { get; set; }
        public string SttNhom { get; set; }
        public string NhomMe { get; set; }
        public bool? CongSl { get; set; }
        public string TkVt { get; set; }
        public string TkDt { get; set; }
        public string TkDtnb { get; set; }
        public string TkGv { get; set; }
        public string TkTl { get; set; }
        public string TkCk { get; set; }
        public bool? Ksd { get; set; }
        public DateTime? CDate { get; set; }
        public DateTime? LDate { get; set; }
        public string CUser { get; set; }
        public string LUser { get; set; }
    }
}
