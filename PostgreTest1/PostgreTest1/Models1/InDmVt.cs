﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class InDmVt
    {
        public string Id { get; set; }
        public string MaCty { get; set; }
        public string MaVt { get; set; }
        public string Loai { get; set; }
        public string TenVt { get; set; }
        public string PartNo { get; set; }
        public string Dvt { get; set; }
        public string DvtBan { get; set; }
        public decimal? HsDvtban { get; set; }
        public string DvtMua { get; set; }
        public decimal? HsDvtmua { get; set; }
        public bool? TonKho { get; set; }
        public string GiaTon { get; set; }
        public string TkVt { get; set; }
        public string TkDt { get; set; }
        public string TkDtnb { get; set; }
        public string TkGv { get; set; }
        public string TkTl { get; set; }
        public string TkCk { get; set; }
        public string TkKm { get; set; }
        public string TkDd { get; set; }
        public string TkCpnvl { get; set; }
        public string MaThue { get; set; }
        public bool? TinhGt { get; set; }
        public string MaNhvt { get; set; }
        public string MaPlvt1 { get; set; }
        public string MaPlvt2 { get; set; }
        public string MaPlvt3 { get; set; }
        public string MaNhvat { get; set; }
        public decimal? SlMin { get; set; }
        public decimal? SlMax { get; set; }
        public int? TsNk { get; set; }
        public int? TsGtgt { get; set; }
        public int? TsXk { get; set; }
        public int? TsTtdb { get; set; }
        public string MaKho { get; set; }
        public string MaVitri { get; set; }
        public bool? CoCt { get; set; }
        public string GhiChu { get; set; }
        public string NhaSx { get; set; }
        public string NhaPp { get; set; }
        public string NuocSx { get; set; }
        public bool? Ksd { get; set; }
        public string CUser { get; set; }
        public string LUser { get; set; }
        public DateTime? CDate { get; set; }
        public DateTime? LDate { get; set; }
    }
}
