﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class VoucherInfo
    {
        public string VoucherCode { get; set; }
        public string PhTableName { get; set; }
        public string CtTableName { get; set; }
        public string CpTableName { get; set; }
        public string Menuid { get; set; }
        public string PhExtraEditCotrolList { get; set; }
        public string SearchClassName { get; set; }
        public string ViewClassName { get; set; }
        public string PrintClassName { get; set; }
        public string MaNtDefault { get; set; }
        public string PhCarryFieldList { get; set; }
        public string CtCarryFieldList { get; set; }
        public string PhPrintFieldList { get; set; }
        public int? RowPerPage { get; set; }
        public int? NumberOfCopy { get; set; }
        public bool? HasCp { get; set; }
        public bool? HasTain { get; set; }
        public bool? HasTaout { get; set; }
        public bool? CopyEnabled { get; set; }
        public bool? CopyVaora { get; set; }
        public string Nxt { get; set; }
        public string Description { get; set; }
    }
}
