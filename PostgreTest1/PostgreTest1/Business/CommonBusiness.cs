﻿
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using PostgreTest1.Models;
using PostgreTest1.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PostgreTest1.Business
{
    public static class CommonBusiness
    {
        public static bool CheckDictCodeExist(ref ApiResult apiResult, string tableName, string codeName, string value, string? id, db_ver10Context context, ILogger logger)
        {
            var result = true;
            dynamic resultQuery;
            dynamic jsonResult;
            var query = "";

            if (string.IsNullOrEmpty(id))
                query = String.Format("select * from check_dict_code_exist('{0}', '{1}', '{2}', '')", tableName, codeName, value);
            else
                query = String.Format("select * from check_dict_code_exist('{0}', '{1}', '{2}', '{3}')", tableName, codeName, value, id);

            resultQuery = context.ExecSQL2(query).FirstOrDefault();
            jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(resultQuery));
            if (jsonResult["check_dict_code_exist"] == 1)
            {
                result = false;
                CommonHelper.setCommonErrorResponse(ref apiResult, codeName + " đã tồn tại!", logger);
            }
            else if (jsonResult["check_dict_code_exist"] != 0)
            {
                result = false;
                CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult["check_dict_code_exist"].ToString(), logger);
            }

            return result;
        }

        public static string GetSoCt()
        {
            return string.Empty;
        }
    }
}
