﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using log4net.Core;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using PostgreTest1.Business;
using PostgreTest1.Models;
using PostgreTest1.Utils;

namespace PostgreTest1.Controllers
{
    [Route("api/dict")]
    [ApiController]
    public class DictionaryController : ControllerBase
    {
        private readonly db_ver10Context _context;
        private readonly ILogger<DictionaryController> _logger;        
        public DictionaryController(db_ver10Context context, ILogger<DictionaryController> logger)
        {
            this._context = context;
            this._logger = logger;
        }

        // GET: api/InDmVt
        [HttpPost]
        [Route("get/{code_name}")]
        public ActionResult<object> GetDatas(string code_name, [FromBody] object paramObj)
        {
            var param = paramObj.ToString();
            ApiResult apiResult = new ApiResult();
            try 
            {
                var dictionary = _context.DictionaryInfos.Where(p => p.CodeName.ToLower() == code_name.ToLower()).FirstOrDefault();
                if (dictionary != null)
                {
                    var daoInfo = _context.DaoInfos.Where(p => p.TableName.ToLower() == dictionary.TableName.ToLower()).FirstOrDefault();
                    if (daoInfo != null)
                    {
                        var getFunction = daoInfo.GetSp.ToLower();
                        var query = String.Format("select * from {0}('{1}')", getFunction, param);
                        var result = _context.ExecSQL2(query);                        
                        return result.ToList();
                    }
                    else
                    {
                        CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin DAO!", _logger);
                        return apiResult;
                    }                        
                }
                else
                {
                    CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin danh mục!", _logger);
                    return apiResult;
                }                    
            }
            catch(Exception ex)
            {
                CommonHelper.setCommonErrorResponse(ref apiResult, ex.Message, _logger);
                return apiResult;
            }            
        }

        // PUT: api/InDmVt/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut]
        [Route("update/{code_name}")]
        public ActionResult<object> PutData(string code_name, [FromBody] object paramObj)
        {
            ApiResult apiResult = new ApiResult();
            try
            {                
                var dictionary = _context.DictionaryInfos.Where(p => p.CodeName.ToLower() == code_name.ToLower()).FirstOrDefault();
                if (dictionary != null)
                {
                    var daoInfo = _context.DaoInfos.Where(p => p.TableName.ToLower() == dictionary.TableName.ToLower()).FirstOrDefault();
                    if (daoInfo != null)
                    {
                        var query = "";
                        dynamic result;
                        dynamic jsonResult;
                        var json = JToken.Parse(paramObj.ToString());
                        //check code field
                        if (!string.IsNullOrEmpty(dictionary.CodeFname))
                        {
                            //check empty
                            if (string.IsNullOrEmpty(json[dictionary.CodeFname].ToString()))
                            {
                                CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa nhập " + dictionary.CodeFname, _logger);
                                return apiResult;
                            }

                            //check exist code                            
                            var checkDictCodeExist = CommonBusiness.CheckDictCodeExist(ref apiResult, dictionary.TableName, dictionary.CodeFname, json[dictionary.CodeFname].ToString(), json["id"].ToString(), _context, _logger); 
                            if (!checkDictCodeExist)
                                return apiResult;
                        }

                        var updFunction = daoInfo.UpdSp.ToLower();
                        query = String.Format("select * from {0}('{1}')", updFunction, paramObj.ToString());
                        result = _context.ExecSQL2(query).FirstOrDefault();
                        jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                        if (jsonResult[updFunction] == 1)
                        {
                            apiResult.data = true;
                            return apiResult;
                        }
                        else
                        {
                            CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult[updFunction].ToString(), _logger);
                            return apiResult;
                        }                            
                    }
                    else
                    {
                        CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin DAO!", _logger);
                        return apiResult;
                    }                        
                }
                else
                {
                    CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin danh mục!", _logger);
                    return apiResult;
                }                    
            }
            catch(Exception ex)
            {
                CommonHelper.setCommonErrorResponse(ref apiResult, ex.Message, _logger);
                return apiResult;
            }
        }

        // POST: api/InDmVt
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Route("insert/{code_name}")]
        public ActionResult<object> PostData(string code_name, [FromBody] object paramObj)
        {
            ApiResult apiResult = new ApiResult();
            var dictionary = _context.DictionaryInfos.Where(p => p.CodeName.ToLower() == code_name.ToLower()).FirstOrDefault();
            if (dictionary != null)
            {
                var daoInfo = _context.DaoInfos.Where(p => p.TableName.ToLower() == dictionary.TableName.ToLower()).FirstOrDefault();
                if (daoInfo != null)
                {
                    var query = "";
                    dynamic result;
                    dynamic jsonResult;
                    var json = JToken.Parse(paramObj.ToString());                    
                    json["id"] = Guid.NewGuid().ToString(); 
                                        
                    try 
                    {
                        //check code field
                        if (dictionary.AutoCreateCode.HasValue && dictionary.AutoCreateCode.Value && !string.IsNullOrEmpty(dictionary.CodeFname) && string.IsNullOrEmpty(json[dictionary.CodeFname].ToString()))
                        {
                            query = String.Format("select * from get_dict_code('{0}')", code_name);
                            result = _context.ExecSQL2(query).FirstOrDefault();
                            jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                            if (!string.IsNullOrEmpty(jsonResult["get_dict_code"].ToString()))
                            {
                                json[dictionary.CodeFname] = jsonResult["get_dict_code"];
                            }
                        }
                        else if ((!dictionary.AutoCreateCode.HasValue || !dictionary.AutoCreateCode.Value) && !string.IsNullOrEmpty(dictionary.CodeFname) && string.IsNullOrEmpty(json[dictionary.CodeFname].ToString()))
                        {
                            CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa nhập " + dictionary.CodeFname, _logger);
                            return apiResult;
                        }

                        if (!string.IsNullOrEmpty(dictionary.CodeFname))
                        {
                            //check exist code                            
                            var checkDictCodeExist = CommonBusiness.CheckDictCodeExist(ref apiResult, dictionary.TableName, dictionary.CodeFname, json[dictionary.CodeFname].ToString(), string.Empty, _context, _logger);
                            if (!checkDictCodeExist)
                                return apiResult;
                        } 
                        
                        //check field required
                        if (!string.IsNullOrEmpty(dictionary.ColumnCheckRequire))
                        {
                            var checkFieldRequired = CheckFieldRequired(dictionary.ColumnCheckRequire, json, ref apiResult);
                            if (!checkFieldRequired)
                                return apiResult;
                        }

                        //check field unique or exist
                        if (!string.IsNullOrEmpty(dictionary.ColumnCheckExists))
                        {
                            var CheckFieldExist = CheckFieldExisted(dictionary.ColumnCheckExists, json, ref apiResult, false, dictionary);
                            if (!CheckFieldExist)
                                return apiResult;
                        }

                        //insert
                        var insFunction = daoInfo.InsSp.ToLower();
                        query = String.Format("select * from {0}('{1}')", insFunction, json.ToString());
                        result = _context.ExecSQL2(query).FirstOrDefault();
                        jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                        if (jsonResult[insFunction] == 1)
                        {
                            apiResult.data = json["id"].ToString();
                            return apiResult;
                        }
                        else
                        {
                            CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult[insFunction].ToString(), _logger);
                            return apiResult;
                        }
                    }
                    catch(Exception ex)
                    {                        
                        CommonHelper.setCommonErrorResponse(ref apiResult, ex.Message, _logger);
                        return apiResult;
                    }                                        
                }
                else
                {
                    CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin DAO!", _logger);
                    return apiResult;                    
                }
                    
            }
            else
            {
                CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin danh mục!", _logger);
                return apiResult;
            }                
        }

        [HttpDelete]
        [Route("delete/{code_name}")]
        public ActionResult<object> DeleteData(string code_name, [FromBody] object paramObj)
        {
            ApiResult apiResult = new ApiResult();
            try
            {
                var dictionary = _context.DictionaryInfos.Where(p => p.CodeName.ToLower() == code_name.ToLower()).FirstOrDefault();
                if (dictionary != null)
                {
                    var daoInfo = _context.DaoInfos.Where(p => p.TableName.ToLower() == dictionary.TableName.ToLower()).FirstOrDefault();
                    if (daoInfo != null)
                    {
                        var json = JToken.Parse(paramObj.ToString());
                        var insFunction = daoInfo.DelSp.ToLower();
                        var query = String.Format("select * from {0}('{1}')", insFunction, json.ToString());
                        dynamic result = _context.ExecSQL2(query).FirstOrDefault();
                        var jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                        if (jsonResult[insFunction] == 1)
                        {
                            apiResult.data = json["id"].ToString();
                            return apiResult;
                        }
                        else
                        {
                            CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult[insFunction].ToString(), _logger);
                            return apiResult;
                        }
                    }
                    else
                    {
                        CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin DAO!", _logger);
                        return apiResult;
                    }
                }
                else
                {
                    CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin danh mục!", _logger);
                    return apiResult;
                }                    
            }
            catch(Exception ex) {
                CommonHelper.setCommonErrorResponse(ref apiResult, ex.Message, _logger);
                return apiResult;
            }
            
        }

        private bool CheckFieldRequired(string listColumn, JToken json, ref ApiResult apiResult)
        {
            var result = true;
            var columnsSplit = listColumn.Split(",");
            if (columnsSplit != null && columnsSplit.Length > 0)
            {
                foreach(string column in columnsSplit)
                {
                    if (string.IsNullOrEmpty(json[column.Trim()].ToString()))
                    {
                        result = false;
                        CommonHelper.setCommonErrorResponse(ref apiResult, column.Trim() + " là thông tin bắt buộc!", _logger);
                        break;
                    }
                }
            }
            return result;
        }

        private bool CheckFieldExisted(string listColumn, JToken json, ref ApiResult apiResult, bool update, DictionaryInfo dictionary)
        {
            var result = true;
            var columnsSplit = listColumn.Split(",");
            if (columnsSplit != null && columnsSplit.Length > 0)
            {                
                foreach (string column in columnsSplit)
                {
                    if (!string.IsNullOrEmpty(json[column.Trim()].ToString()))
                    {       
                        if (!update)
                            result = CommonBusiness.CheckDictCodeExist(ref apiResult, dictionary.TableName, column.Trim(), json[column.Trim()].ToString(), "", _context, _logger);
                        else
                            result = CommonBusiness.CheckDictCodeExist(ref apiResult, dictionary.TableName, column.Trim(), json[column.Trim()].ToString(), json["id"].ToString(), _context, _logger);
                        if (!result)
                            break;
                    }
                }
            }
            return result;
        }
    }
}
