﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PostgreTest1.Models;
using PostgreTest1.Utils;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace PostgreTest1.Controllers
{
    [Route("api/sys")]
    [ApiController]
    public class SysController : ControllerBase
    {
        private readonly db_ver10Context _context;
        private readonly ILogger<SysController> _logger;
        public SysController(db_ver10Context context, ILogger<SysController> logger)
        {
            this._context = context;
            this._logger = logger;
        }

        // GET: api/InDmVt
        [HttpGet]
        [Route("get/login")]
        public async Task<ActionResult<object>> GetInitInfomations()
        {            
            ApiResult apiResult = new ApiResult();
            try
            {
                var menu = _context.MenuInfos.Where(p => p.Used == true).ToList();
                var config = _context.SiSetupInfos.ToList();
                dynamic data = new ExpandoObject();
                data.menu = menu;
                data.config = config;
                apiResult.data = data;
                return apiResult;
            }
            catch (Exception ex)
            {
                CommonHelper.setCommonErrorResponse(ref apiResult, ex.Message, _logger);
                return apiResult;
            }
        }
    }
}
