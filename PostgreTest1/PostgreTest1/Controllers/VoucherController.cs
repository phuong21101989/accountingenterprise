﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PostgreTest1.Models;
using PostgreTest1.Utils;

namespace PostgreTest1.Controllers
{
    [Route("api/voucher")]
    [ApiController]
    public class VoucherController : ControllerBase
    {
        private readonly db_ver10Context _context;
        private readonly ILogger<VoucherController> _logger;
        public VoucherController(db_ver10Context context, ILogger<VoucherController> logger)
        {
            _context = context;
            _logger = logger;
        }

        // GET: api/InDmVt
        [HttpPost]
        [Route("get/{code_name}")]
        public ActionResult<object> GetDatas(string code_name, [FromBody] object paramObj)
        {
            ApiResult apiResult = new ApiResult();
            var param = paramObj.ToString();
            //get voucher info
            var voucher = _context.VoucherInfos.Where(p => p.VoucherCode.ToLower() == code_name.ToLower()).FirstOrDefault();
            if (voucher != null)
            {
                //get dao info of ph data
                var daoInfoPH = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.PhTableName.ToLower()).FirstOrDefault();
                if (daoInfoPH != null)
                {
                    //ph Get function
                    var getPHFunction = daoInfoPH.GetSp.ToLower();
                    var query = String.Format("select * from {0}('{1}')", getPHFunction, param);
                    var listPH = _context.ExecSQL2(query).ToList();
                    // get dao info of ct data
                    var daoInfoCT = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.CtTableName.ToLower()).FirstOrDefault();
                    //ct Get function
                    var getCTFunction = daoInfoCT.GetSp.ToLower();
                    dynamic jResult = new System.Dynamic.ExpandoObject();
                    jResult.data = listPH;
                    foreach (dynamic ph in jResult.phData)
                    {
                        dynamic paramCT = new System.Dynamic.ExpandoObject();
                        paramCT.id_ph = ph.id;
                        var paramCTStr = JsonConvert.SerializeObject(paramCT);
                        query = String.Format("select * from {0}('{1}')", getCTFunction, paramCTStr);
                        ph.ctData = _context.ExecSQL2(query).ToList();
                    }
                    return jResult;
                }
                else
                {
                    CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin DAO!", _logger);
                    return apiResult;
                }
                    
            }
            else
            {
                CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin phiếu!", _logger);
                return apiResult;
            }                
        }

        [HttpPut]
        [Route("update/{code_name}")]
        public ActionResult<object> PutInDmVt(string code_name, [FromBody] object paramObj)
        {
            ApiResult apiResult = new ApiResult();
            var voucher = _context.VoucherInfos.Where(p => p.VoucherCode.ToLower() == code_name.ToLower()).FirstOrDefault();
            
            if (voucher != null)
            {
                var daoInfoPH = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.PhTableName.ToLower()).FirstOrDefault();
                if (daoInfoPH != null)
                {
                    var daoInfoCT = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.CtTableName.ToLower()).FirstOrDefault();
                    var json = JToken.Parse(paramObj.ToString());

                    if (json["phData"] != null)
                    {
                        var jsonPH = json["phData"];
                        if (jsonPH["ma_ct"] == null || string.IsNullOrEmpty(jsonPH["ma_ct"].ToString()))
                            jsonPH["ma_ct"] = code_name;
                        //check so_ct
                        var query = "";
                        dynamic jsonResult;                                                    
                        //common.validateRequire
                        //common.validateExist
                        var updPHFunction = daoInfoPH.UpdSp.ToLower();
                        query = String.Format("select * from {0}('{1}')", updPHFunction, jsonPH.ToString());
                        var insCTFunction = daoInfoCT.InsSp.ToLower();
                        var delCTFunction = daoInfoCT.DelSp.ToLower();
                        JArray jsonCT = (JArray)json["ctData"];

                        using (IDbContextTransaction transaction = _context.Database.BeginTransaction())
                        {
                            try
                            {
                                dynamic result = _context.ExecSQL2(query).FirstOrDefault();
                                jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                if (jsonResult[updPHFunction] != 1)
                                {
                                    transaction.Rollback();
                                    CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult[updPHFunction].ToString(), _logger);
                                    return apiResult;
                                }

                                if (jsonCT != null)
                                {
                                    query = String.Format("select * from {0}('{1}')", delCTFunction, jsonPH.ToString());
                                    result = _context.ExecSQL2(query).FirstOrDefault();
                                    jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                    if (jsonResult[delCTFunction] != 1)
                                    {
                                        transaction.Rollback();
                                        CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult[delCTFunction].ToString(), _logger);
                                        return apiResult;
                                    }
                                    foreach (var ct in jsonCT)
                                    {
                                        //common.validateRequire
                                        //common.validateExist
                                        ct["id"] = Guid.NewGuid().ToString();
                                        query = String.Format("select * from {0}('{1}')", insCTFunction, ct.ToString());
                                        result = _context.ExecSQL2(query).FirstOrDefault();
                                        jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                        if (jsonResult[insCTFunction] != 1)
                                        {
                                            transaction.Rollback();
                                            CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult[insCTFunction].ToString(), _logger);
                                            return apiResult;
                                        }
                                    }

                                    //if ()
                                }

                                //get process function
                                var sict = _context.SiDmCts.Where(p => p.MaCt.ToLower() == code_name.ToLower()).FirstOrDefault();
                                var ctProcessFuction = sict.SpProcess.ToLower();
                                jsonPH["mode"] = "2";
                                query = String.Format("select * from {0}('{1}')", ctProcessFuction, jsonPH.ToString());
                                result = _context.ExecSQL2(query).FirstOrDefault();
                                transaction.Commit();                                                                
                                apiResult.data = true;
                                return apiResult;
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                CommonHelper.setCommonErrorResponse(ref apiResult, ex.Message, _logger);
                                return apiResult;
                            }
                        }

                    }
                    else
                    {
                        CommonHelper.setCommonErrorResponse(ref apiResult, "Không có thông tin phiếu!", _logger);
                        return apiResult;
                    }                        
                }
                else
                {
                    CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin DAO!", _logger);
                    return apiResult;
                }                    
            }
            else
            {
                CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin phiếu!", _logger);
                return apiResult;
            }                         
        }
        
        [HttpPost]
        [Route("insert/{code_name}")]
        public ActionResult<object> PostInDmVt(string code_name, [FromBody] object paramObj)
        {
            ApiResult apiResult = new ApiResult();
            var voucher = _context.VoucherInfos.Where(p => p.VoucherCode.ToLower() == code_name.ToLower()).FirstOrDefault();
            if (voucher != null)
            {
                var daoInfoPH = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.PhTableName.ToLower()).FirstOrDefault();
                if (daoInfoPH != null)
                {
                    var daoInfoCT = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.CtTableName.ToLower()).FirstOrDefault();
                    var json = JToken.Parse(paramObj.ToString());

                    if (json["phData"] != null)
                    {
                        var jsonPH = json["phData"];
                       
                        jsonPH["id"] = Guid.NewGuid().ToString();
                        if (jsonPH["ma_ct"] == null || string.IsNullOrEmpty(jsonPH["ma_ct"].ToString()))
                            jsonPH["ma_ct"] = code_name;
                        var query = "";
                        dynamic jsonResult;
                        if (jsonPH["so_ct"] == null || string.IsNullOrEmpty(jsonPH["so_ct"].ToString()))
                        {
                            query = String.Format("select * from get_soct('{0}')", code_name);
                            dynamic result = _context.ExecSQL2(query).FirstOrDefault();
                            jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));

                            jsonPH["so_ct"] = jsonResult["get_soct"];
                        }

                        //common.validateRequire
                        //common.validateExist

                        //GetSoCT if so_ct = null or so_ct = ''
                        var insPHFunction = daoInfoPH.InsSp.ToLower();                        
                        query = String.Format("select * from {0}('{1}')", insPHFunction, jsonPH.ToString());
                        var insCTFunction = daoInfoCT.InsSp.ToLower();
                        JArray jsonCT = (JArray)json["ctData"];                        

                        using (IDbContextTransaction transaction = _context.Database.BeginTransaction())
                        {
                            try
                            {
                                dynamic result = _context.ExecSQL2(query).FirstOrDefault();
                                jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                if (jsonResult[insPHFunction] != 1)
                                {
                                    transaction.Rollback();
                                    CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult[insPHFunction], _logger);
                                    return apiResult;
                                }

                                if (jsonCT != null)
                                {                                    
                                    foreach (var ct in jsonCT)
                                    {
                                        if (ct["id"] == null || string.IsNullOrEmpty(ct["id"].ToString()))
                                            ct["id"] = Guid.NewGuid().ToString();

                                        ct["id_ph"] = jsonPH["id"];
                                        //common.validateRequire
                                        //common.validateExist
                                        query = String.Format("select * from {0}('{1}')", insCTFunction, ct.ToString());
                                        result = _context.ExecSQL2(query).FirstOrDefault();
                                        jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                        if (jsonResult[insCTFunction] != 1)
                                        {
                                            transaction.Rollback();
                                            CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult[insCTFunction], _logger);
                                            return apiResult;
                                        }
                                    }                                    
                                }

                                //get process function
                                var sict = _context.SiDmCts.Where(p => p.MaCt.ToLower() == code_name.ToLower()).FirstOrDefault();
                                var ctProcessFuction = sict.SpProcess.ToLower();
                                jsonPH["mode"] = "1";
                                query = String.Format("select * from {0}('{1}')", ctProcessFuction, jsonPH.ToString());
                                result = _context.ExecSQL2(query).FirstOrDefault();
                                transaction.Commit();                                
                                apiResult.data = true;                                
                                return apiResult;
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                CommonHelper.setCommonErrorResponse(ref apiResult, ex.Message, _logger);
                                return apiResult;
                            }
                        }

                    }
                    else
                    {
                        CommonHelper.setCommonErrorResponse(ref apiResult, "Không có thông tin phiếu!", _logger);
                        return apiResult;
                    }                                                          
                }
                else
                {
                    CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin DAO!", _logger);
                    return apiResult;
                }                    
            }
            else
            {
                CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin phiếu!", _logger);
                return apiResult;
            }                
        }

        [HttpDelete]
        [Route("delete/{code_name}")]
        public ActionResult<object> DeleteData(string code_name, [FromBody] object paramObj)
        {
            ApiResult apiResult = new ApiResult();
            try
            {
                var voucher = _context.VoucherInfos.Where(p => p.VoucherCode.ToLower() == code_name.ToLower()).FirstOrDefault();
                if (voucher != null)
                {
                    var daoInfo = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.PhTableName.ToLower()).FirstOrDefault();
                    if (daoInfo != null)
                    {
                        var json = JToken.Parse(paramObj.ToString());
                        var insFunction = daoInfo.DelSp.ToLower();
                        var query = String.Format("select * from {0}('{1}')", insFunction, json.ToString());
                        dynamic result = _context.ExecSQL2(query).FirstOrDefault();
                        var jsonResult = JToken.Parse(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                        if (jsonResult[insFunction] == 1)
                        {
                            apiResult.data = json["id"].ToString();
                            return apiResult;
                        }
                        else
                        {
                            CommonHelper.setCommonErrorResponse(ref apiResult, jsonResult[insFunction].ToString(), _logger);
                            return apiResult;
                        }
                    }
                    else
                    {
                        CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin DAO!", _logger);
                        return apiResult;
                    }
                }
                else
                {
                    CommonHelper.setCommonErrorResponse(ref apiResult, "Bạn chưa khai báo thông tin phiếu!", _logger);
                    return apiResult;
                }
            }
            catch (Exception ex)
            {
                CommonHelper.setCommonErrorResponse(ref apiResult, ex.Message, _logger);
                return apiResult;
            }

        }
    }
}
