﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PostgreTest1.Models;

namespace PostgreTest1.Controllers
{
    [Route("api/vf")]
    [ApiController]
    public class ViewFormatController : ControllerBase
    {
        private readonly db_ver10Context _context;
        public ViewFormatController(db_ver10Context context)
        {
            _context = context;
        }

        // GET: api/InDmVt
        [HttpPost]
        [Route("get")]
        public async Task<ActionResult<IEnumerable<ViewFormat>>> GetDatas([FromBody] object paramObj)
        {
            var param = paramObj.ToString();
            var json = JObject.Parse(param);
            IQueryable<ViewFormat> result = _context.ViewFormats;
            if (json["code_name"] != null && !string.IsNullOrEmpty(json["code_name"].ToString()))
            {
                result = result.Where(p => p.CodeName == json["code_name"].ToString());
            }

            if (json["ma_mau"] != null && !string.IsNullOrEmpty(json["ma_mau"].ToString()))
            {
                result = result.Where(p => p.CodeName == json["ma_mau"].ToString());
            }

            return await result.OrderBy(p => p.FieldOrder).ToListAsync();            
        }

        [HttpPost]
        [Route("import")]
        public async Task<ActionResult<object>> ImportAsync(List<ViewFormat> viewFormats)
        {
            foreach(ViewFormat viewFormat in viewFormats)
            {
                viewFormat.Id = Guid.NewGuid().ToString();
                _context.ViewFormats.Add(viewFormat);
                try
                {
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateException)
                {
                    if (ViewFormatExists(viewFormat.CodeName))
                    {
                        return Conflict();
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            dynamic jResult = new System.Dynamic.ExpandoObject();
            jResult.data = true;

            return jResult;
        }

        [HttpPut]
        [Route("update/{code_name}")]
        public ActionResult<object> PutInDmVt(string code_name, [FromBody] object paramObj)
        {
            var voucher = _context.VoucherInfos.Where(p => p.VoucherCode.ToLower() == code_name.ToLower()).FirstOrDefault();
            if (voucher != null)
            {
                var daoInfoPH = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.PhTableName.ToLower()).FirstOrDefault();
                if (daoInfoPH != null)
                {
                    var daoInfoCT = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.PhTableName.ToLower()).FirstOrDefault();
                    var json = JToken.Parse(paramObj.ToString());

                    if (json["phData"] != null)
                    {
                        var jsonPH = json["phData"];
                        //common.validateRequire
                        //common.validateExist
                        var updPHFunction = daoInfoPH.UpdSp.ToLower();
                        var query = String.Format("select * from {0}('{1}')", updPHFunction, jsonPH.ToString());
                        var updCTFunction = daoInfoCT.UpdSp.ToLower();
                        JArray jsonCT = (JArray)json["ctData"];

                        using (IDbContextTransaction transaction = _context.Database.BeginTransaction())
                        {
                            try
                            {
                                dynamic result = _context.ExecSQL2(query).FirstOrDefault();
                                if (result[updPHFunction] != 1)
                                {
                                    transaction.Rollback();
                                    return BadRequest("Có lỗi xảy ra!");
                                }

                                if (jsonCT != null)
                                {
                                    foreach (var ct in jsonCT)
                                    {
                                        //common.validateRequire
                                        //common.validateExist
                                        query = String.Format("select * from {0}('{1}')", updCTFunction, ct.ToString());
                                        result = _context.ExecSQL2(query).FirstOrDefault();
                                        if (result[updCTFunction] != 1)
                                        {
                                            transaction.Rollback();
                                            return BadRequest("Có lỗi xảy ra!");
                                        }
                                    }
                                }

                                transaction.Commit();
                                dynamic jResult = new System.Dynamic.ExpandoObject();
                                jResult.result = true;
                                return jResult;
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                Console.WriteLine("Error occurred.");
                                return BadRequest("Có lỗi xảy ra!");
                            }
                        }

                    }
                    else
                        return BadRequest("Không có thông tin phiếu!");
                }
                else
                    return BadRequest("Bạn chưa khai báo thông tin DAO!");
            }
            else
                return BadRequest("Bạn chưa khai báo thông tin phiếu!");            
        }

        [HttpPost]
        [Route("insert/{code_name}")]
        public ActionResult<object> PostInDmVt(string code_name, [FromBody] object paramObj)
        {            
            var voucher = _context.VoucherInfos.Where(p => p.VoucherCode.ToLower() == code_name.ToLower()).FirstOrDefault();
            if (voucher != null)
            {
                var daoInfoPH = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.PhTableName.ToLower()).FirstOrDefault();
                if (daoInfoPH != null)
                {
                    var daoInfoCT = _context.DaoInfos.Where(p => p.TableName.ToLower() == voucher.PhTableName.ToLower()).FirstOrDefault();
                    var json = JToken.Parse(paramObj.ToString());

                    if (json["phData"] != null)
                    {
                        var jsonPH = json["phData"];
                        if (jsonPH["id"] == null || string.IsNullOrEmpty(jsonPH["id"].ToString()))
                            jsonPH["id"] = Guid.NewGuid().ToString();
                        //common.validateRequire
                        //common.validateExist
                        //GetSoCT if so_ct = null or so_ct = ''
                        var insPHFunction = daoInfoPH.InsSp.ToLower();                        
                        var query = String.Format("select * from {0}('{1}')", insPHFunction, jsonPH.ToString());
                        var insCTFunction = daoInfoCT.InsSp.ToLower();
                        JArray jsonCT = (JArray)json["ctData"];                        

                        using (IDbContextTransaction transaction = _context.Database.BeginTransaction())
                        {
                            try
                            {
                                dynamic result = _context.ExecSQL2(query).FirstOrDefault();
                                if (result[insPHFunction] != 1)
                                {
                                    transaction.Rollback();
                                    return BadRequest("Có lỗi xảy ra!");
                                }

                                if (jsonCT != null)
                                {                                    
                                    foreach (var ct in jsonCT)
                                    {
                                        if (ct["id"] == null || string.IsNullOrEmpty(ct["id"].ToString()))
                                            ct["id"] = Guid.NewGuid().ToString();
                                        //common.validateRequire
                                        //common.validateExist
                                        query = String.Format("select * from {0}('{1}')", insCTFunction, ct.ToString());
                                        result = _context.ExecSQL2(query).FirstOrDefault();
                                        if (result[insCTFunction] != 1)
                                        {
                                            transaction.Rollback();
                                            return BadRequest("Có lỗi xảy ra!");
                                        }
                                    }                                    
                                }

                                transaction.Commit();
                                dynamic jResult = new System.Dynamic.ExpandoObject();
                                jResult.result = true;
                                return jResult;
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                Console.WriteLine("Error occurred.");
                                return BadRequest("Có lỗi xảy ra!");
                            }
                        }

                    }
                    else
                        return BadRequest("Không có thông tin phiếu!");                                        
                }
                else
                    return BadRequest("Bạn chưa khai báo thông tin DAO!");
            }
            else
                return BadRequest("Bạn chưa khai báo thông tin phiếu!");
        }

        // DELETE: api/InDmVt/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteInDmVt(string id)
        {
            var InDmVt = await _context.InDmVts.FindAsync(id);
            if (InDmVt == null)
            {
                return NotFound();
            }

            _context.InDmVts.Remove(InDmVt);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool ViewFormatExists(string code_name)
        {
            return _context.ViewFormats.Any(e => e.CodeName == code_name);
        }
    }
}
