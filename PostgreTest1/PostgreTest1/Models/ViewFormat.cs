﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class ViewFormat
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string CodeName { get; set; }
        public string Menuid { get; set; }
        public string MaMau { get; set; }
        public string Language { get; set; }
        public string FieldName { get; set; }
        public string FieldType { get; set; }
        public string FieldTitle { get; set; }
        public int? FieldOrder { get; set; }
        public string FieldFormat { get; set; }
        public bool? FieldHide { get; set; }
        public decimal? FieldWidth { get; set; }
        public short? ViewType { get; set; }
        public bool? DefaultSort { get; set; }
        public bool? Locked { get; set; }
    }
}
