﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class InCt1
    {
        public string Id { get; set; }
        public string MaCty { get; set; }
        public string IdPh { get; set; }
        public int? Stt { get; set; }
        public DateTime? NgayCt { get; set; }
        public string IdLsx { get; set; }
        public int? SttLsx { get; set; }
        public string SoLsx { get; set; }
        public string MaKho { get; set; }
        public string MaVt { get; set; }
        public string TenVt { get; set; }
        public string Dvt { get; set; }
        public string TkVt { get; set; }
        public string TkCo { get; set; }
        public string MaNx { get; set; }
        public decimal? SoLuong { get; set; }
        public decimal? SoLuongQd { get; set; }
        public decimal? GiaNt { get; set; }
        public decimal? Gia { get; set; }
        public decimal? TienNt { get; set; }
        public decimal? Tien { get; set; }
        public string MaVitri { get; set; }
        public string MaLo { get; set; }
        public string MaHd { get; set; }
        public string MaBp { get; set; }
        public string MaPhi { get; set; }
        public string MaSpct { get; set; }
    }
}
