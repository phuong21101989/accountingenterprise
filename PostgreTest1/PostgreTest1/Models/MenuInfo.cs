﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class MenuInfo
    {
        public string Menuid { get; set; }
        public string Stt { get; set; }
        public string Type { get; set; }
        public string Moduleid { get; set; }
        public string Bar { get; set; }
        public string ShortName { get; set; }
        public string DllName { get; set; }
        public string Command { get; set; }
        public string AngularController { get; set; }
        public string AngularTemplate { get; set; }
        public string AngularFilter { get; set; }
        public string AngularRoute { get; set; }
        public string CodeName { get; set; }
        public bool? Report { get; set; }
        public bool? Basicright { get; set; }
        public bool? Form { get; set; }
        public string FormShowType { get; set; }
        public string Opt { get; set; }
        public string Picture1 { get; set; }
        public string Picture2 { get; set; }
        public int? Rowid { get; set; }
        public bool? Used { get; set; }
        public string Active { get; set; }
    }
}
