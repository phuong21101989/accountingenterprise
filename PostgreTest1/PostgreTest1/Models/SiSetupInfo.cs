﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class SiSetupInfo
    {
        public string MaCty { get; set; }
        public string SetupCode { get; set; }
        public string SetupName { get; set; }
        public string SetupValue { get; set; }
        public string SetupType { get; set; }
    }
}
