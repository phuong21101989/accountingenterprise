﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class GlCt
    {
        public string MaCty { get; set; }
        public string IdPh { get; set; }
        public int Stt { get; set; }
        public int? Nam { get; set; }
        public int? Thang { get; set; }
        public string MaCt { get; set; }
        public DateTime? NgayCt { get; set; }
        public DateTime? NgayLct { get; set; }
        public DateTime? NgayLo { get; set; }
        public string SoCt { get; set; }
        public string SoLo { get; set; }
        public string NguoiGd { get; set; }
        public string DienGiai { get; set; }
        public string Tk { get; set; }
        public string TkDu { get; set; }
        public string MaNx { get; set; }
        public decimal? PsCoNt { get; set; }
        public decimal? PsNoNt { get; set; }
        public decimal? PsCo { get; set; }
        public decimal? PsNo { get; set; }
        public decimal? TyGia { get; set; }
        public string MaNt { get; set; }
        public string NhomDk { get; set; }
        public string MaKh { get; set; }
        public string MaNvkd { get; set; }
        public string MaKu { get; set; }
        public string MaLo { get; set; }
        public string MaHd { get; set; }
        public string MaBp { get; set; }
        public string MaPhi { get; set; }
        public string MaSpct { get; set; }
        public DateTime? Cdate { get; set; }
        public string Cuser { get; set; }
        public DateTime? Ldate { get; set; }
        public string Luser { get; set; }
    }
}
