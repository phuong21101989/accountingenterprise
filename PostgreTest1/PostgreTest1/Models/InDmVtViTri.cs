﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class InDmVtViTri
    {
        public string Id { get; set; }
        public string MaCty { get; set; }
        public string MaKho { get; set; }
        public string TenVitri { get; set; }
        public string MaVitri { get; set; }
        public bool? Ksd { get; set; }
        public DateTime? CDate { get; set; }
        public DateTime? LDate { get; set; }
        public string CUser { get; set; }
        public string LUser { get; set; }
    }
}
