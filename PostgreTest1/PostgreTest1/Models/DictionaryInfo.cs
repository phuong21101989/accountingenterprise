﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class DictionaryInfo
    {
        public string CodeName { get; set; }
        public string Pk { get; set; }
        public string CodeFname { get; set; }
        public int? CodeLength { get; set; }
        public string NameFname { get; set; }
        public string TableName { get; set; }
        public string Menuid { get; set; }
        public bool? LookupWhenInvalid { get; set; }
        public bool? AllowMergeCode { get; set; }
        public string Dllname { get; set; }
        public string ViewClassName { get; set; }
        public string EditClassName { get; set; }
        public string CarryFieldList { get; set; }
        public string DefaultSort { get; set; }
        public bool? CopyVaora { get; set; }
        public string ColumnCheckExists { get; set; }
        public string ColumnCheckRelate { get; set; }
        public string ColumnCheckRequire { get; set; }
        public string Description { get; set; }
        public string Prefix { get; set; }
        public bool? AutoCreateCode { get; set; }
    }
}
