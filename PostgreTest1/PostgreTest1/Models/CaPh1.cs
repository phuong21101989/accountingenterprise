﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class CaPh1
    {
        public string Id { get; set; }
        public string MaCty { get; set; }
        public string SoCt { get; set; }
        public DateTime? NgayCt { get; set; }
        public DateTime? NgayLct { get; set; }
        public string MaKh { get; set; }
        public string TenKh { get; set; }
        public string DiaChi { get; set; }
        public string NguoiGd { get; set; }
        public string DienGiai { get; set; }
        public string TkNo { get; set; }
        public string MaNt { get; set; }
        public decimal? TyGia { get; set; }
        public decimal? TTienNt { get; set; }
        public decimal? TTien { get; set; }
        public string TrangThai { get; set; }
        public bool? Post2gl { get; set; }
        public DateTime? CDate { get; set; }
        public DateTime? LDate { get; set; }
        public string CUser { get; set; }
        public string LUser { get; set; }
        public string MaCt { get; set; }
    }
}
