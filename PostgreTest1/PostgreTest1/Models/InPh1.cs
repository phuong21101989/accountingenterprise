﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class InPh1
    {
        public string Id { get; set; }
        public string MaCty { get; set; }
        public string MaGd { get; set; }
        public DateTime? NgayCt { get; set; }
        public DateTime? NgayLct { get; set; }
        public string SoCt { get; set; }
        public string MaKh { get; set; }
        public string TenKh { get; set; }
        public string DiaChi { get; set; }
        public string NguoiGd { get; set; }
        public string DienGiai { get; set; }
        public string MaBp { get; set; }
        public string MaNx { get; set; }
        public string MaPhi { get; set; }
        public string MaNt { get; set; }
        public decimal? TyGia { get; set; }
        public decimal? TTienNt { get; set; }
        public decimal? TTien { get; set; }
        public decimal? TSoLuong { get; set; }
        public bool? PnGtb { get; set; }
        public bool? GiaThanh { get; set; }
        public string TrangThai { get; set; }
        public bool? Post2gl { get; set; }
        public bool? Post2in { get; set; }
        public DateTime? CDate { get; set; }
        public DateTime? LDate { get; set; }
        public string CUser { get; set; }
        public string LUser { get; set; }
    }
}
