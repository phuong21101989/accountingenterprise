﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class InDmVtDmKho
    {
        public string Id { get; set; }
        public string MaCty { get; set; }
        public string MaKho { get; set; }
        public string TenKho { get; set; }
        public bool? KhoDl { get; set; }
        public string TkDl { get; set; }
        public int? SttNtxt { get; set; }
        public string DiaChi { get; set; }
        public string NguoiLh { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public bool? Ksd { get; set; }
        public DateTime? CDate { get; set; }
        public DateTime? LDate { get; set; }
        public string CUser { get; set; }
        public string LUser { get; set; }
    }
}
