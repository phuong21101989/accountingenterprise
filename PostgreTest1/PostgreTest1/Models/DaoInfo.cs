﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PostgreTest1.Models
{
    public partial class DaoInfo
    {
        public string TableName { get; set; }
        public string GetSp { get; set; }
        public string InsSp { get; set; }
        public string UpdSp { get; set; }
        public string DelSp { get; set; }
        public string SchSp { get; set; }
        public string ChgSp { get; set; }
        public string Description { get; set; }
    }
}
